require 'optparse'
require 'csv'
require 'fseq4r'

options = {t: 1}
opt = OptionParser.new do |opts|
  opts.banner = "Usage: #{File.basename($PROGRAM_NAME)} [options]"

  #
  # Specific options
  #
  opts.separator ""
  opts.separator "Specific Options:"

  #
  # F-sequence order
  #
  opts.on('-n N', Integer, 'Order of f-sequence.') do |n|
    options[:n] = n
  end

  #
  # Seed for pseudorandom number generator (PRNG)
  #
  opts.on('-s SEED', Integer, 'Seed for pseudorandom number generator.') do |s|
    options[:seed] = s
  end

  #
  # Number of experiments per sample
  #
  opts.on('-t T', Integer, 'Number of experiments per sample.') do |t|
    options[:t] = t
  end

  #
  # Verbose output
  #
  opts.on('-v', '--[no]-verbose', 'Verbose output.') do
    options[:verbose] = true
  end

  #
  # Help message
  #
  opts.on('-h', '--help', 'Show this message.') do
    STDERR.puts opts
    exit
  end

  #
  # Parse command-line arguments
  #
  begin
    # Parse arguments
    opts.parse!

    # Validate command-line arguments
    if options[:n].nil?
      STDERR.puts 'ERROR: F-sequence order must be set.'
      STDERR.puts opts
      exit 1
    end
  rescue
    # Print arguments
    STDERR.puts opts

    # Exit with errors
    exit 1
  end
end



#
# Set parameters
#
n    = options[:n]                       # F-sequence dimension
t    = options[:t]                       # Experiments per sample
seed = options[:seed] || Random.new_seed # Seed for PRNG
vrb  = options[:verbose]                 # Verbose output
prng = Random.new(seed)



#
# Run t times DAC pseudorandom fsequences generation
#
t.times do
  fs = Fseq4r::dac_gen_rand_fs(n, prng)
  puts fs.to_csv(converters: :integer)
end
