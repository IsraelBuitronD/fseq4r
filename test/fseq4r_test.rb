require 'test_helper'

class Fseq4rTest < Minitest::Test
  def setup
    # F-sequences of order 4
    @fseq_n4 = [
      [1,2,1,3,1,2,1,4,1,2,1,3,1,2,1,4],
      [1,2,1,3,2,1,2,4,1,2,1,3,2,1,2,4],
      [1,2,1,3,2,1,2,4,2,1,2,3,1,2,1,4],
      [1,2,1,3,2,1,2,4,1,3,1,2,3,1,3,4],
      [1,2,1,3,2,1,2,4,3,1,3,2,1,3,1,4],
      [1,2,1,3,1,4,3,2,3,4,1,4,2,3,2,4],
      [1,2,1,3,1,2,4,3,1,2,1,3,1,2,4,3],
      [1,2,1,3,1,2,1,4,2,3,1,3,2,3,1,4],
      [1,2,1,3,1,2,4,2,1,3,1,2,1,3,4,3]
    ]

    # F-sequences of order 4
    @idxs_count_fseq_n4 = [
      [8,4,2,2],
      [6,6,2,2],
      [6,6,2,2],
      [6,4,4,2],
      [6,4,4,2],
      [4,4,4,4],
      [6,4,4,2],
      [6,4,4,2],
      [6,4,4,2]
    ]

    @tc_gen_random_fsequence = [
      # n | seed | expected f-sequence
      [  3,     1, [1,3,2,3,1,3,2,3]],
      [  3,     2, [1,2,3,2,1,2,3,2]],
      [  4,     1, [1,3,1,2,3,4,3,1,4,3,4,2,3,1,3,4]],
      [  4,     2, [1,2,3,1,4,1,2,4,1,4,3,1,2,1,4,2]]
    ]
  end



  def test_that_it_has_a_version_number
    refute_nil ::Fseq4r::VERSION
  end



  def test_has_valid_symbols
    # When n<3
    assert_raises(ArgumentError) { ::Fseq4r::has_valid_symbols?([1,2,1,2],2) }
    # When n is not Integer
    assert_raises(ArgumentError) { ::Fseq4r::has_valid_symbols?([1,2,1,2],'2') }
    # When seq is empty
    assert_raises(ArgumentError) { ::Fseq4r::has_valid_symbols?([],3) }
    # When invalid symbol in sequence
    refute ::Fseq4r::has_valid_symbols?([1,2,1,4],3)
    # When valid sequence
    assert ::Fseq4r::has_valid_symbols?([1,2,1,3,1,2,1,3],3)
  end



  def test_parity
    assert_equal [], ::Fseq4r::parity([])
    assert_equal [0], ::Fseq4r::parity([0])
    assert_equal [1], ::Fseq4r::parity([0,1,0])
    assert_equal [2], ::Fseq4r::parity([0,1,0,2,0,1,0])
    assert_equal [0,1,2], ::Fseq4r::parity([0,1,0,2,0])
  end



  def test_search_item_for_index
    seq = [1,2,1,3,1,2,1,3]
    exp = [[1,2,3],[2,3],[1,3],[3],[1,2],[2],[1],[3]]
    (0...seq.size).each do |i|
      assert_equal exp[i], ::Fseq4r::search_item_for_index(seq,3,i)
    end
  end



  def test_is_fsequence
    # When n<3
    assert_raises(ArgumentError) { ::Fseq4r::is_fsequence?([1,2,1,2],2) }
    # When n is not Integer
    assert_raises(ArgumentError) { ::Fseq4r::is_fsequence?([1,2,1,2],'2') }
    # When valid sequence
    assert ::Fseq4r::is_fsequence?([1,2,1,3,1,2,1,3],3)
    assert ::Fseq4r::is_fsequence?([1,2,1,3,1,2,1,4,1,2,1,3,1,2,1,4],4)
  end



  def test_are_fsequences_from_csv
    # When not valid n
    assert_raises(ArgumentError) { ::Fseq4r::are_fsequences_from_csv?('./test/fseq_n3.csv',2) }

    # When csv file with valid fsequences
    assert ::Fseq4r::are_fsequences_from_csv?('./test/fseq_n3.csv',3)
    assert ::Fseq4r::are_fsequences_from_csv?('./test/fseq_n4.csv',4)
  end



  def test_tau2
    # When a simple rotation
    assert_equal [4,5,6,7,8,1,2,3], ::Fseq4r::tau2([1,2,3,4,5,6,7,8],5)
    # When a simple rotation is greater than sequence size
    assert_equal [8,1,2,3,4,5,6,7], ::Fseq4r::tau2([1,2,3,4,5,6,7,8],9)
  end



  def test_tau3
    # When sequence
    assert_equal [3,2,3,1,3,2,3,1], ::Fseq4r::tau3([1,2,1,3,1,2,1,3],3)
  end



  def test_tauP
    # When computed permutation is a valid one
    n=3
    perms = (1..n).to_a.permutation.to_a
    p = ::Fseq4r::tauP(3)
    assert perms.include? p
  end



  def test_eval
    n,fseq = 3,[1,2,1,3,1,2,1,3]
    t = [
      [Fseq4r::TAU_1,[fseq,n]],
      [Fseq4r::TAU_2,[nil,n,5]],
      [Fseq4r::TAU_3,[nil,n]],
    ]
    expected = [3,1,3,2,3,1,3,2]

    result = ::Fseq4r::eval(t)
    assert_equal expected,result
  end



  def test_load_t_from_yaml
    # When file does not exist
    assert_raises(ArgumentError) { ::Fseq4r::load_t_from_yaml('nofile.yml') }

    # When valid yaml descriptor file
    result   = ::Fseq4r::load_t_from_yaml('./test/ts_eval.yml')
    expected = [[1,[[1,2,1,3,1,2,1,3],3]],[2,["nil",3,5]],[3,["nil",3]]]
    assert_equal expected,result
  end



  def test_apply_perm
    # When sequence
    f = [1,2,1,3,1,2,1,3]
    p = [2,1,3]
    assert_equal [2,1,2,3,2,1,2,3], ::Fseq4r::apply_perm(f,3,p)
  end



  def test_tau4
    # When sequence
    f = [1,2,1,3,1,2,1,3]
    g = ::Fseq4r::tau4(f,3)
    assert ::Fseq4r::is_fsequence?(g,3)
  end

  def test_gen_gray_code
    # When n<1
    assert_raises(ArgumentError) {::Fseq4r::gen_gray_code(0)}
    # When n is not integer
    assert_raises(ArgumentError) {::Fseq4r::gen_gray_code('a')}
    # When n is a positive integer
    assert_equal [0,1], ::Fseq4r::gen_gray_code(1)
    assert_equal [0,2,3,1], ::Fseq4r::gen_gray_code(2)
    assert_equal [0,4,6,2,3,7,5,1], ::Fseq4r::gen_gray_code(3)
  end

  def test_indices_seq
    # When n<1
    assert_raises(ArgumentError) {::Fseq4r::indices_seq(0)}
    # When n is not integer
    assert_raises(ArgumentError) {::Fseq4r::indices_seq('a')}
    # When n is a positive integer
    assert_equal [1], ::Fseq4r::indices_seq(1)
    assert_equal [1,2,1], ::Fseq4r::indices_seq(2)
    assert_equal [1,2,1,3,1,2,1], ::Fseq4r::indices_seq(3)
  end

  def test_apply_indices_seq
  	# When sequence is empty
    assert_equal [0], ::Fseq4r::apply_indices_seq(0,[])
    # When sequence is not empty
    assert_equal [0,1], ::Fseq4r::apply_indices_seq(0,[1])
    assert_equal [0,1,3,2], ::Fseq4r::apply_indices_seq(0,[1,2,1])
    assert_equal [0,1,3,2,6,7,5,4], ::Fseq4r::apply_indices_seq(0,[1,2,1,3,1,2,1])
  end

  def test_has_even_parity
    # Refute empty sequences
    refute ::Fseq4r::has_even_parity?([])

    # Pass on odd_parity sequences
    assert ::Fseq4r::has_even_parity?([1])
    assert ::Fseq4r::has_even_parity?([1,2])
    assert ::Fseq4r::has_even_parity?([1,2,1])

    # Refute on even_parity sequences
    refute ::Fseq4r::has_even_parity?([1,1])
    refute ::Fseq4r::has_even_parity?([1,2,2,1])
    refute ::Fseq4r::has_even_parity?([1,2,3,2,3,1])
  end

  def test_indices
    assert_empty ::Fseq4r::indices(1,[])
    assert_equal [0], ::Fseq4r::indices(1,[1])
    assert_equal [0,4,7,9,10], ::Fseq4r::indices(1,[1,2,3,4,1,2,3,1,2,1,1])
  end

  def test_is_rotation
    # Sequeces which are rotations
    assert ::Fseq4r::is_rotation?([],[])
    assert ::Fseq4r::is_rotation?([1,2,1],[1,2,1])
    assert ::Fseq4r::is_rotation?([1,2,1,3],[3,1,2,1])
    assert ::Fseq4r::is_rotation?([1,1,2],[2,1,1])

    # Sequeces which are not rotations
    refute ::Fseq4r::is_rotation?([1,2,1],[1,2])
    refute ::Fseq4r::is_rotation?([],[1])
    refute ::Fseq4r::is_rotation?([1],[])
  end

  def test_find_pt1s
    # When n is not positive integer
    assert_raises(ArgumentError) {::Fseq4r::find_pt1s([1],0)}
    assert_raises(ArgumentError) {::Fseq4r::find_pt1s([1],2.0)}
    # When wrong sequence size
    assert_raises(ArgumentError) {::Fseq4r::find_pt1s([1,2,1,3],2)}

    # When sequences has not pt1s
    assert_empty ::Fseq4r::find_pt1s([1,2,1,3,1,2,1],3)
    # When sequences has pt1s
    assert_equal [[[2,3,4],[2,1,3,1,4,1,2,4],[4,2],3],[[2,3],[3,2,1,3,1,4,1,2],[3,4,2],4]], ::Fseq4r::find_pt1s([2,3,4,3,2,1,3,1,4,1,2,4,3,4,2],4)
  end

  def test_pt1
    #
    assert_equal [2,3,4,1,4,2,1,4,1,3,1,2,1,4,2], ::Fseq4r::pt1([2,3,4],[2,1,3,1,4,1,2,4],[4,2],3)
    # Exceptions
    assert_raises(ArgumentError) {::Fseq4r::pt1([1,2,1],[],[1,2,1],1)}
  end

  def test_find_pt2s
    # Sequences with feasible pt2s
    assert_empty ::Fseq4r::find_pt2s([2,3,4,1,4,2,1,4,1,3,1,2,1,4,2],4)
    assert_equal [[2,4,6,10],[4,8,10,12]], ::Fseq4r::find_pt2s([4,3,2,3,4,3,2,1,2,3,4,3,2,3,4],4)
    # Exceptions
    assert_raises(ArgumentError) {::Fseq4r::find_pt2s([1,2,1,3,1,4,2,1,3,1,2,1,4,2,3,1],4)}
  end

  def test_count_indices_in_seq
    # F-sequences of order 4
    @fseq_n4.each_index do |i|
      assert_equal @idxs_count_fseq_n4[i], ::Fseq4r::count_indices_in_seq(@fseq_n4[i],4)
    end
  end



  def test_gen_random_fsequence
    @tc_gen_random_fsequence.each do |tc|
      assert_equal tc[2], ::Fseq4r::gen_random_fsequence(tc[0],tc[1])
    end
  end

  def test_search_min_fs
    assert_equal [1,2,1,3,1,2,1,3], Fseq4r::search_min_fs(3,[3,1,3,2,3,1,3,2])
    assert_equal [1,2,1,3,1,2,1,3], Fseq4r::search_min_fs(3,[2,3,1,3,2,3,1,3])
    assert_equal [1,2,1,3,1,2,1,4,1,2,1,3,1,2,1,5,1,2,1,3,1,2,1,4,1,2,1,3,1,2,1,5], Fseq4r::search_min_fs(5,[1,2,3,2,4,2,3,2,5,2,3,2,4,2,3,2,1,2,3,2,4,2,3,2,5,2,3,2,4,2,3,2])
    assert_equal [1,2,1,3,1,2,1,4,1,2,1,3,1,5,1,3,1,2,1,4,1,2,1,3,1,2,1,4,1,5,1,4], Fseq4r::search_min_fs(5,[1,2,3,2,4,2,5,2,4,2,3,2,4,2,5,2,1,2,5,2,4,2,3,2,4,2,5,2,4,2,3,2])
    assert_equal [1,2,1,3,1,2,1,4,1,2,1,3,1,2,1,4], Fseq4r::search_min_fs(4,[1,2,1,3,1,2,1,4,1,2,1,3,1,2,1,4])
    assert_equal [1,2,1,3,1,2,1,4,1,3,2,3,1,3,2,4], Fseq4r::search_min_fs(4,[1,2,1,3,1,2,1,4,2,3,1,3,2,3,1,4])
    assert_equal [1,2,1,3,1,4,3,2,3,4,1,4,2,3,2,4], Fseq4r::search_min_fs(4,[1,2,1,3,4,1,4,2,4,3,2,1,2,3,4,3])
  end

  def test_group_by_freqs
    assert_equal [[1],[2,3]], Fseq4r::group_by_freqs([4,2,2])
    assert_equal [[2],[1,3]], Fseq4r::group_by_freqs([2,4,2])
    assert_equal [[1],[2,3],[4]], Fseq4r::group_by_freqs([6,4,4,2])
    assert_equal [[1,2],[3,4]], Fseq4r::group_by_freqs([6,6,2,2])
    assert_equal [[4],[1],[3],[2,5]], Fseq4r::group_by_freqs([10,2,6,12,2])
  end

  def test_frequencies
    assert_equal [4,2,2], Fseq4r::frequencies([1,2,1,3,1,2,1,3],3)
    assert_equal [2,4,2], Fseq4r::frequencies([2,3,2,1,2,3,2,1],3)
    assert_equal [0,4,2,2], Fseq4r::frequencies([2,3,2,4,2,3,2,4],4)
  end

  def test_permutations_by_groups
    assert_equal [[1,2,3],[1,3,2]], Fseq4r::permutations_by_groups([[1],[2,3]])
    assert_equal [[2,1,3],[2,3,1]], Fseq4r::permutations_by_groups([[2],[1,3]])
    assert_equal [[1,2,3],[2,1,3]], Fseq4r::permutations_by_groups([[1,2],[3]])
    assert_equal [[1,2,3,4],[1,3,2,4]], Fseq4r::permutations_by_groups([[1],[2,3],[4]])
    assert_equal [[1,2,3,4],[1,2,4,3],[2,1,3,4],[2,1,4,3]], Fseq4r::permutations_by_groups([[1,2],[3,4]])
    assert_equal [[4,1,3,2,5],[4,1,3,5,2]], Fseq4r::permutations_by_groups([[4],[1],[3],[2,5]])
    assert_equal [[4,1,3,2,5],[4,1,3,5,2],[1,4,3,2,5],[1,4,3,5,2]], Fseq4r::permutations_by_groups([[4,1],[3],[2,5]])
  end
end
