require 'optparse'
require 'csv'
require 'fseq4r'

def count_odd_parity(fs, i, j, n)
  # Initialize
  odds_counter = 0;
  counter = Array.new(n) { |i| i = 0 }

  # Sum each number in subsequence from i to j indices
  (i..j).each { |l|  counter[fs[l]] += 1 }

  # Count how many sums are odd
  (0...n).each { |l|  odds_counter += 1  if counter[l] % 2 == 1 }

  odds_counter
end

def search_squares(fs, n)
  sqr_counter = 0
  two_to_n = (1 << n)
  sqrs = []

  (0...(two_to_n-1)).each do |i|
    ((i+1)...two_to_n).each do |j|
      if fs[i] == fs[j]
        # Look for twisted square
        if count_odd_parity(fs, i, j-1, n) == 1
          # Increment square counter
          sqr_counter += 1
          # Mark seq[i] and seq[j-1] as part of a square
          sqrs << [1,i,j]
          next
        end

        # Look for straight square
        if count_odd_parity(fs, i, j, n) == 1
          # Increment square counter
          sqr_counter += 1
          # Mark s[i] and s[j-1] as part of a square
          sqrs << [0,i,j-1]
          next
        end

      end
    end
  end

  sqrs
end

options = {}
opt = OptionParser.new do |opts|
  opts.banner = "Usage: #{File.basename($PROGRAM_NAME)} [options]"

  #
  # Specific options
  #
  opts.separator ""
  opts.separator "Specific Options:"

  #
  # F-sequence
  #
  opts.on('-s FS', Array, 'F-sequence.') do |s|
    options[:fs] = s.map { |e| e.to_i }
  end

  #
  # Help message
  #
  opts.on('-h', '--help', 'Show this message.') do
    STDERR.puts opts
    exit
  end

  opts.separator ""
  opts.separator "Output Format: it is in comma-separated format with 5 columns:"
  opts.separator "    SQRTYPE: 0 is a straight square or 1 is a twisted square."
  opts.separator "    VI:      Vertex v_i."
  opts.separator "    VJ:      Vertex v_j."
  opts.separator "    I:       Index of v_i vertex in hamiltonian cycle."
  opts.separator "    J:       Index of v_j vertex in hamiltonian cycle."

  #
  # Parse command-line arguments
  #
  begin
    # Parse arguments
    opts.parse!

    # Validate sequence presence
    if options[:fs].nil?
      STDERR.puts 'ERROR: F-sequence must be set.'
      STDERR.puts opts
      exit 1
    end

    # Validate sequence not empty
    if options[:fs].empty?
      STDERR.puts 'ERROR: F-sequence must not be empty.'
      STDERR.puts "ERROR: F-sequence size #{options[:fs].size}."
      STDERR.puts opts
      exit 1
    end

    # Validate sequence length
    log2 = Math.log2(options[:fs].size)
    if log2 - log2.floor > 0
      STDERR.puts 'ERROR: F-sequence length must be a power of two.'
      STDERR.puts opts
      exit 1
    else
      options[:n] = log2.floor
    end

  rescue
    # Print arguments
    STDERR.puts opts

    # Exit with errors
    exit 1
  end
end


#
# Set parameters
#
n    = options[:n]   # F-sequence dimension
fs   = options[:fs]  # F-sequence
h    = [0]           # Hamiltonian cycle

# Compute hamiltonian cycle
fs.each { |e|  h << (h.last ^ (1 << e)) }
h.pop

# Search squares
puts ['SQRTYPE','VI','VJ','I','J'].to_csv
sqrs = search_squares(fs,n)
sqrs.each do |sq|
  sq << h.index(sq[1]) << h.index(sq[2])
  puts sq.to_csv
end
