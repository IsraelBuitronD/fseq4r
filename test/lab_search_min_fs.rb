require 'optparse'
require 'csv'
require 'fseq4r'

options = { t: 1, break: false, check: false }
OptionParser.new do |opts|
  opts.banner = "Usage: #{File.basename($PROGRAM_NAME)} [options]"

  #
  # Specific options
  #
  opts.separator ''
  opts.separator 'Specific Options:'

  #
  # F-sequence order
  #
  opts.on('-n N', Integer, 'Order of f-sequence.') do |n|
    options[:n] = n
  end

  #
  # Seed for pseudorandom number generator (PRNG)
  #
  opts.on('-s SEQ', Array, 'F-sequence to minimize.') do |s|
    # options[:seq] = s.map { |e| e.to_i }
    options[:seq] = s.map(&:to_csv)
  end

  #
  # Validate well-formed sequences
  #
  opts.on('-c', '--[no-]check', 'Set to validate well-formation.') do
    options[:check] = true
  end

  #
  # Break on non well-formed sequence
  #
  opts.on('-b', '--[no-]break',
          'Set to break is a non well-formed sequence.') do
    options[:break] = true
  end

  #
  # CSV file as dataset of f-sequences
  #
  opts.on('-f CSVFILE', String, 'CSV file as dataset of f-sequences.') do |f|
    options[:csv] = f
  end

  #
  # Help message
  #
  opts.on('-h', '--help', 'Show this message.') do
    STDERR.puts opts
    exit
  end

  #
  # Parse command-line arguments
  #
  is_error = false

  # Parse arguments
  opts.parse!

  # Validate f-sequence order from command-line arguments
  if options[:n].nil?
    STDERR.puts 'ERROR: F-sequence order must be set.'
    is_error = true
  # Validate f-sequence order from command-line arguments
  elsif !options[:n].between?(2, 10)
    STDERR.puts 'ERROR: F-sequence order must be between 2 and 10.'
    is_error = true
  # Validate presence of -s and -f flags in command-line arguments
  elsif !(options[:seq].nil? ^ options[:csv].nil?)
    STDERR.puts 'ERROR: Either -s or -f flag must be set.'
    is_error = true
  # CSV file as dataset of f-sequences.
  elsif !options[:csv].nil? && !File.file?(options[:csv])
    STDERR.puts "ERROR: File `#{options[:csv]}` must exists."
    is_error = true
  end

  if is_error
    STDERR.puts opts
    exit 1
  end
end

#
# Set parameters
#
n  = options[:n]         # F-sequence order
fs = options[:seq]       # F-sequence array
csvfile = options[:csv]  # CSV file as dataset of f-sequences.

#
# Run search of minimum f-sequence
#
if options[:seq]
  if options[:check]
    check = Fseq4r.is_fsequence?(fs, n)
    if options[:break] && !check
      # Print arguments
      STDERR.puts "NOT-VALID\t#{fs.to_csv(converters: :integer)}"
      exit 1 # Exit with errors
    end
  end
  minimum = Fseq4r.search_min_fs(n, fs)
  puts minimum.to_csv(converters: :integer)
  return
end

if options[:check]
  CSV.foreach(csvfile, converters: :integer) do |row|
    check = Fseq4r.is_fsequence?(row, n)
    if check
      minimum = Fseq4r.search_min_fs(n, row)
      puts minimum.to_csv(converters: :integer)
    else
      # Print arguments
      STDERR.puts "NOT-VALID\t#{fs.to_csv(converters: :integer)}"
      exit 1 if options[:break] # Exit with errors
      next                      # Continue with next sequence
    end
  end
end
