# Fseq4r

Fseq4r es una biblioteca de Ruby para usar secuencias-f.

## Instalaci&oacute;n

Agrege la siguiente l&iacute;nea al archivo `Gemfile` de su aplicaci&oacute;n:

```ruby
gem 'fseq4r'
```

y ejecute:

    $ bundle

O puede instarlo manualmente con el comando:

    $ gem install fseq4r

## Uso

Esta gema define un m&oacute;dulo llamado `Fseq4r` el cual contiene muchos m&eacute;todos --de Ruby-- para manejar secuencias-f. Puede revisar el archivo `test/fseq4r_test.rb` para obtener algunos ejemplos.

## Desarrollo

Esta biblioteca de Ruby es parte de mi trabajo de investigaci&oacute;n de mis estudios doctorales. Actualmente se encuentra en desarrollo y cualquier contribuci&oacute;n ser&aacute; agradecida.

Despu&eacute;s de obtener el repositorio, ejecute `bin/setup` para instalar las dependencias. Luego, ejecute `rake test` para verificar los casos de prueba. Tambi&eacute;n puede ejecutar `bin/console` para tener una l&iacute;nea de comando interactiva para cualquier experimento.

Para instalar esta gema en su equipo local, ejecute, `bundle exec rake install`. Para publicar una nueva versi&oacute;n, actualice el n&uacute;mero de versi&oacute;n en el archivo `version.rb` y ejecute `bundle exec rake release`, esto creara una nueva etiqueta (*tags*) de git para la nueva versi&oacute;n, env&iacute;enos sus *commits* y etiquetas (*tags*) y finalmente publique su archivo `.gem` en [rubygems.org].

## Autores

* **Israel Buitr&oacute;n** - *Estudiante doctorado* - [P&aacute;gina Web](http://computacion.cs.cinvestav.mx/~ibuitron) - ibuitron@computacion.cs.cinvestav.mx
* **Feli&uacute; Sagols** - *Director de tesis* - [P&aacute;gina Web](http://www.math.cinvestav.mx/fsagols) - fsalgols@math.cinvestav.mx
* **Guillermo Morales** - *Director de tesis* - [P&aacute;gina Web](https://www.cs.cinvestav.mx/en/Investigadores/Imorales) - gmorales@cs.cinvestav.mx

## Contribuci&oacute;n

Los reportes de errores, parches de c&oacute;digo (*pull requests*) son bienvenidas en GitLab en la direcci&oacute;n [gitlab.com/israelbuitron/fseq4r][gitlab]. Este proyecto. Este proyecto pretende ser un espacio seguro y agradable para la colaboraci&oacute;n y se espera que los contribuyentes se adhieran al C&oacute;digo de Conducta.

[rubygems.org]: https://rubygems.org
[gitlab]: https://gitlab.com/israelbuitron/fseq4r
