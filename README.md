# Fseq4r

Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/fseq4r`. To experiment with that code, run `bin/console` for an interactive prompt.

TODO: Delete this and the text above, and describe your gem

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'fseq4r'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install fseq4r

## Usage

TODO: Write usage instructions here

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Authors

* **Israel Buitr&oacute;n** - *PhD student* - [Webpage](http://computacion.cs.cinvestav.mx/~ibuitron) - ibuitron@computacion.cs.cinvestav.mx
* **Feli&uacute; Sagols** - *PhD thesis advisor* - [Webpage](http://www.math.cinvestav.mx/fsagols) - fsalgols@math.cinvestav.mx
* **Guillermo Morales** - *PhD thesis advisor* - [Webpage](https://www.cs.cinvestav.mx/en/Investigadores/Imorales) - gmorales@cs.cinvestav.mx

## Contributing

Bug reports and pull requests are welcome on GitLab at [gitlab.com/israelbuitron/fseq4r][gitlab]. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

[gitlab]: https://gitlab.com/israelbuitron/fseq4r
